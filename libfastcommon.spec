Name: libfastcommon
Version: 1.0.72
Release: 1
Summary: c common functions library extracted from my open source projects FastDFS
License: LGPL-2.0-or-later
Group: Arch/Tech
URL:  https://github.com/happyfish100/libfastcommon
Source: https://github.com/happyfish100/libfastcommon/archive/V%{version}.tar.gz

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n) 

BuildRequires: cmake gcc libcurl-devel
Requires: libcurl

%description
c common functions library extracted from my open source projects FastDFS.
this library is very simple and stable. functions including: string, logger,
chain, hash, socket, ini file reader, base64 encode / decode,
url encode / decode, fasttimer etc. 

%package devel
Summary: Development header file
Requires: libcurl-devel
Requires: %{name}%{?_isa} = %{version}-%{release}

%description devel
This package provides the header files of libfastcommon

%prep
%autosetup -n %{name}-%{version} 

%build
./make.sh clean && ./make.sh

%install
rm -rf %{buildroot}
DESTDIR=$RPM_BUILD_ROOT ./make.sh install
ln -sf /usr/lib64/libfastcommon.so %{buildroot}/usr/lib/libfastcommon.so

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_usr}/lib64/libfastcommon.so*
%{_usr}/lib/libfastcommon.so*

%files devel
%defattr(-,root,root,-)
%{_usr}/include/fastcommon/*

%changelog
* Mon Feb 26 2024 liweigang <izmirvii@gmail.com> - 1.0.72-1
- update to version 1.0.72

* Mon Mar 20 2023 suweifeng <suweifeng1@huawei.com> - 1.0.66-1
- Upgrade to version 1.0.66

* Fri Dec 16 2022 lihaoxiang <lihaoxiang9@huawei.com> - 1.0.64-1
- Upgrade to version 1.0.64

* Tue May 31 2022 Zhiqiang Liu <liuzhiqiang26@huawei.com> - 1.0.43-4
- Specify license as LGPL-2.0-or-later

* Mon Jul 5 2021 sunligang <sunligang@kylinos.cn> - 1.0.43-3
- Remove useless readme files

* Wed Jun 23 2021 wangxiaomeng <wangxiaomeng@kylinos.cn> - 1.0.43-2
- Fix version in changelog of spec

* Fri Sep 11 2020 huyan90325 <huyan90325@talkweb.com.cn> - 1.0.43-1
- Package init
